  
  
let configuracionSlideCards =  {
  direction: 'horizontal',
  loop: true,
  breakpoints: {
      320: {
          centeredSlides: true,
          slidesPerView: 1.2,
          spaceBetween: 10
      },
      640: {
        slidesPerView: 1.5,
        centeredSlides: false,
        spaceBetween: 15
      }
    }
  
}

  let swiper = new Swiper('.swipper-1',configuracionSlideCards);
  let swiper3 = new Swiper('.swipper-3', configuracionSlideCards);

  let swiper2 = new Swiper('.swipper-2', {
    direction: 'horizontal',
    centeredSlides: true,
    loop: true,
    spaceBetween:10,
    navigation: {
      nextEl: "#next",
      prevEl: "#prev",
    },
    autoplay: {
        delay: 3000,
      },
      breakpoints: {
        320: {
            centeredSlides: true,
            slidesPerView: 1
        },
        640: {
          slidesPerView: 2,
          centeredSlides: false,
          spaceBetween: 10
        },
        942: {
            slidesPerView: 2.7,
            centeredSlides: true,
            spaceBetween: 5,
            
          }
      },
      on: {
        init: function () {
            let swipperFrame = document.querySelector(".swiper-slide-active .frame");
            swipperFrame.classList.add("active");
            
        },
        transitionStart: function() {
          let frames = document.getElementsByClassName("frame");
          for(let i = 0; i < frames.length; i++){
            let currentFrame = frames[i];
            currentFrame.classList.remove("active");
          }
          
        },
        transitionEnd: function(swiper2) {
            let swipperFrame = document.querySelector(".swiper-slide-active .frame");
            swipperFrame.classList.add("active");
        }
      }
    
  });

 