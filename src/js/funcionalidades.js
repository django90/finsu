window.onscroll = headerFixedFunction;

let navbarContainer = document.getElementsByClassName("parent-navbar")[0];
let navbarHeight = navbarContainer.clientHeight; 
let header = document.getElementsByTagName("header")[0]; 
let nextHeaderElement = header.nextElementSibling;
let siblingOffsetTop = nextHeaderElement.offsetTop;

function headerFixedFunction() {
    if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
        navbarContainer.classList.add("header-active");
        if(!isIndex() && !(navbarContainer.classList.contains('fixed-top'))){
            let newOffset = siblingOffsetTop + navbarHeight;
            navbarContainer.classList.add("fixed-top");
            nextHeaderElement.setAttribute('style',`margin-top:${newOffset}px!important`);
        }

    } else {
        if(navbarContainer.classList.contains("fixed-top")){
            navbarContainer.classList.remove("header-active"); 
            // navbarContainer.classList.remove("fixed-top") 
            currentSiblingHeight = nextHeaderElement.offsetTop;
            siblingHeightOriginal = currentSiblingHeight - navbarHeight; 
            
            nextHeaderElement.setAttribute('style',`margin-top:${siblingOffsetTop}px!important`);

        }
    }
}
function getDocPageName(){
    let path = window.location.pathname;
    let page = path.split("/").pop();
    return page;
}
function isIndex(){
    return (getDocPageName()=="index.php")?true:false;
}


let navbarBtn = document.getElementsByClassName("navbar-toggler")[0];
navbarBtn.addEventListener("click",function(){
    document.getElementsByClassName("navigation")[0].parentNode.parentNode.classList.add("header-active");
});
