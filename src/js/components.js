$('.multiple-items').slick({
  dots: false,
  infinite: true,  
  autoplay: true,
  autoplaySpeed: 2000,
  slidesToShow: 5,
  slidesToScroll: 1,
  prevArrow: $('.prev'),
  nextArrow: $('.next'),
  centerMode: true,    
  centerPadding: '30px',         
  variableWidth: false,
  mobileFirst:false,
  responsive: [
    {
      breakpoint: 2000,
      settings: {
          slidesToShow: 3,
      }
      },
      {
      breakpoint: 992,
      settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          swipeToSlide:true,
          centerMode: false,  
      }
      },
      {
      breakpoint: 600,
      settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
              }
      }
  ]
  });



  $('.multiple-cards').slick({
    dots: false,
    infinite: false,  
    autoplay: false,
    focusOnSelect:true,
    slidesToShow: 3,
    slidesToScroll: 1,         
    variableWidth: true,
    swipeToSlide:true,  
    responsive: [
      {
        breakpoint: 2000,
        settings: {
            slidesToShow: 2,
        }
        },
        {
        breakpoint: 600,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
                }
        }
    ]
    });
  