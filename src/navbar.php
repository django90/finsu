<div class="container-fluid d-flex justify-content-center ">
    <div class="container ">
        <div class="row d-flex align-items-center navigation">
        <div class="col-sm-12 col-lg-10">
            <nav class="navbar navbar-expand-lg navbar-light principal">
                <a href="index.php">
                    <img src="./assets/img/finsu.svg" alt="" class="logo navbar-brand">
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#myNavBar" aria-controls="myNavBar" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="myNavBar">
                    <ul class="navbar-nav ms-auto mb-lg-0">
                        <li class="nav-item ms-5">
                            <a class="nav-link" aria-current="page" href="servicios.php">Servicios</a>
                        </li>
                        <li class="nav-item ms-5">
                            <a class="nav-link" href="inversiones.php">Proyectos para invertir</a>
                        </li>
                        <li class="nav-item ms-5">
                            <a class="nav-link" href="nosotros.php">Nosotros</a>
                        </li>
                        <li class="nav-item ms-5">
                            <a class="nav-link" href="blog.php">Blog</a>
                        </li>
                        <li class="nav-item ms-5">
                            <a class="btn btn-primary px-3" href="faq.php" role="button">Contacto</a>
                        </li>
                    </ul>
                </div>
            </nav>                 
        </div>
        <div class="col-lg-2">
            <div class="nav justify-content-evenly secondary-nav d-flex align-items-center">
                <a href="" class="btn btn-primary my-0 py-1 px-2 text-white" >ES</a>
                <a class="icon-Search" href="faq.php"></a>
                <a class="icon-Info" href=""></a>
            </div>
        </div>
        </div>
    </div>
</div>