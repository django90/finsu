
<!DOCTYPE html>
<html lang="es">
<?php include 'head.php';?>
<body>  
    <header>
        <div class="fixed-top parent-navbar">
            <?php include 'navbar.php';?>
        </div>
    </header>
    <main>
        <section class="img-index-background vh-80">
            <div class="container">
                <div class="col-12 col-md-8 col-lg-6">
                    <div class="top-bottom-space">
                        <hgroup>
                            <h1 class="fw-bold">Haz crecer tu dinero con inversiones fáciles y seguras</h1>
                            <h4 class="my-3 pe-5 py-3">Creamos proyectos inmobiliarios excepcionales, con el mejor rendimiento y accesibilidad financiera.</h4>
                        </hgroup>
                        <a href ="inversiones.php" type="button" class="btn btn-primary btn-lg py-3">
                            <div class="append-greater-than" >
                                Conocer oportunidades  
                            </div>
                        </a>
                    </div>
                </div> 
            </div>
            
        </section>




        <article class="container article-1 mt-5">
            <div class="info row my-5 ps-5 pe-5">
                <div class="col-sm-12 col-md-6 mb-3 d-flex align-items-center">
                    <div>
                        <h5 class="text-primary">NOSOTROS</h5>
                        <h3 class="fw-bold">Especialistas en inversiones, área inmobiliaria y urbanización</h3>
                        <p class="py-3">Líderes en proyectos inmobiliarios con experiencia en inversiones éxitosas, tenemos trayectoria en México y
                            Brasil llevando a nuestros socios al siguente nivel.
                        </p>
                        <div class="d-flex flex-wrap py-3">
                            <div class="me-3 "><p class="text-muted">FINSU en menos de 1 minuto</p></div>
                            <div class="mb-3"><a id="verVideo"  href="">Ver video</a></div> 
                            
                        </div>
                        
                        <a href ="inversiones.php" type="button" class="btn btn-primary btn-lg append-greater-than py-3">
                            Ver más
                        </a>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6  d-flex align-items-center">
                    <img src="./assets/img/maquina.png" alt="">
                </div>
            </div>
        </article>
        <article class="article-2">
            <div class="container d-flex flex-column justify-content-center align-items-center pt-5 pb-5">
                <div class="d-flex flex-column justify-content-center align-items-center">
                    <h5 class="text-primary">SERVICIOS</h5>
                    <p class="h3 text-center">Inversiones seguras </p>
                    <p class="h4 fw-bold text-center">
                        Esta es una de las maneras más seguras de hacer crecer tu dinero o patrimonio
                    </p>
                </div>
                <div class="d-flex flex-wrap justify-content-evenly" id="cards">
                    <div class="first">
                        <div class="iconos">
                            <!-- <i class="far fa-hand-point-up"></i> -->
                        </div>
                        <div>
                            <h4 class="pb-3">Asesoramiento</h4>
                            <p>
                                Asesoramos a nuestros clientes para que tomen la mejor decisión redituable
                            </p>
                        </div>
                        <a href="servicios.php">Ver más ></a>
                    </div>
                    <div class="second">
                        <div class="iconos">
                            <!-- <i class="fas fa-users"></i> -->
                        </div>
                        <div>
                            <h4 class="pb-3">Crowdfunding </h4>
                            <p>
                                Inversiones atractivas, tu eliges a tus socios
                            </p>
                        </div>
                        
                        <a href="servicios.php">Ver más ></a>
                    </div>
                    <div class="third">
                        <div class="iconos">
                            <!-- <i class="fas fa-truck"></i> -->
                        </div>
                        <div>
                            <h4 class="pb-3">Construcción y urbanización</h4>
                            <p>
                                Convierte tu sueño en realidad
                            </p>
                        </div>
                        
                        <a href="servicios.php">Ver más ></a>
                    </div>
                </div>
            </div>
        </article>
        <article class ="article-3">
            <div class="d-flex flex-column align-items-center justify-content-center mt-5 mb-5">
                <h3 class="h3 fw-bold">CASOS DE ÉXITO</h4>
                <h4 class="h4 text-center">Hemos concluido desarrollos y proyectos de alto nivel </h4>
            </div>
            <div class="container-fluid">
            <div class="swiper-container swipper-2">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="frame">
                            <img src="./assets/img/carousel/img-1.png" alt="">
                            <div class="bottom-left content">
                                <div class="row">
                                    <div class="col-9 bg-white border-white">
                                        <h4 class="fw-bold">SALVIA</h4>
                                        <h5>Vida en crecimiento</h5>
                                        <p>Complejo de 7 lotes residenciales en Izamal,
                                            Yucatán. Cuentan con un tamaño de 375m2, 
                                            han aumentado un 16% su valor desde la venta 
                                            inicial debido a la plusvalía de la zona. 
                                            Inversionistas de este proyecto ya disfrutan 
                                            de sus rendimientos.
                                        </p>
                                    </div>
                                    <div class="col-3 bg-transparent d-flex align-items-center justify-content-start pt-5 ">
                                        <a class="" href=""> Ver desarrollo </a>   
                                    </div>
                                </div>
                                
                                
                            </div>
                            
                            <div class="top-right">
                                <img src="./assets/img/carousel/SALVIA.svg" alt="">                       
                                </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="frame">
                            <img src="./assets/img/carousel/img-1.png" alt="">
                            <div class="bottom-left content">
                                <div class="row">
                                    <div class="col-9 bg-white border-white">
                                        <h4 class="fw-bold">SALVIA</h4>
                                        <h5>Vida en crecimiento</h5>
                                        <p>Complejo de 7 lotes residenciales en Izamal,
                                            Yucatán. Cuentan con un tamaño de 375m2, 
                                            han aumentado un 16% su valor desde la venta 
                                            inicial debido a la plusvalía de la zona. 
                                            Inversionistas de este proyecto ya disfrutan 
                                            de sus rendimientos.
                                        </p>
                                    </div>
                                    <div class="col-3 bg-transparent d-flex align-items-center justify-content-start pt-5 ">
                                        <a class="" href=""> Ver desarrollo </a>   
                                    </div>
                                </div>
                                
                                
                            </div>
                            <div class="top-right">
                                <img src="./assets/img/carousel/SALVIA.svg" alt="">                       
                                </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="frame">
                            <img src="./assets/img/carousel/img-1.png" alt="">
                            <div class="bottom-left content">
                                <div class="row">
                                    <div class="col-9 bg-white border-white">
                                        <h4 class="fw-bold">SALVIA</h4>
                                        <h5>Vida en crecimiento</h5>
                                        <p>Complejo de 7 lotes residenciales en Izamal,
                                            Yucatán. Cuentan con un tamaño de 375m2, 
                                            han aumentado un 16% su valor desde la venta 
                                            inicial debido a la plusvalía de la zona. 
                                            Inversionistas de este proyecto ya disfrutan 
                                            de sus rendimientos.
                                        </p>
                                    </div>
                                    <div class="col-3 bg-transparent d-flex align-items-center justify-content-start pt-5 ">
                                        <a class="" href=""> Ver desarrollo </a>   
                                    </div>
                                </div>
                                
                                
                            </div>
                            <div class="top-right">
                                <img src="./assets/img/carousel/SALVIA.svg" alt="">                       
                                </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="frame">
                            <img src="./assets/img/carousel/img-1.png" alt="">
                            <div class="bottom-left content">
                                <div class="row">
                                    <div class="col-9 bg-white border-white">
                                        <h4 class="fw-bold">SALVIA</h4>
                                        <h5>Vida en crecimiento</h5>
                                        <p>Complejo de 7 lotes residenciales en Izamal,
                                            Yucatán. Cuentan con un tamaño de 375m2, 
                                            han aumentado un 16% su valor desde la venta 
                                            inicial debido a la plusvalía de la zona. 
                                            Inversionistas de este proyecto ya disfrutan 
                                            de sus rendimientos.
                                        </p>
                                    </div>
                                    <div class="col-3 bg-transparent d-flex align-items-center justify-content-start pt-5 ">
                                        <a class="" href=""> Ver desarrollo </a>   
                                    </div>
                                </div>
                                
                                
                            </div>
                            <div class="top-right">
                                <img src="./assets/img/carousel/SALVIA.svg" alt="">                       
                                </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="frame">
                            <img src="./assets/img/carousel/img-1.png" alt="">
                            <div class="bottom-left content">
                                <div class="row">
                                    <div class="col-9 bg-white border-white">
                                        <h4 class="fw-bold">SALVIA</h4>
                                        <h5>Vida en crecimiento</h5>
                                        <p>Complejo de 7 lotes residenciales en Izamal,
                                            Yucatán. Cuentan con un tamaño de 375m2, 
                                            han aumentado un 16% su valor desde la venta 
                                            inicial debido a la plusvalía de la zona. 
                                            Inversionistas de este proyecto ya disfrutan 
                                            de sus rendimientos.
                                        </p>
                                    </div>
                                    <div class="col-3 bg-transparent d-flex align-items-center justify-content-start pt-5 ">
                                        <a class="" href=""> Ver desarrollo </a>   
                                    </div>
                                </div>
                                
                                
                            </div>
                            
                            <div class="top-right">
                                <img src="./assets/img/carousel/SALVIA.svg" alt="">                       
                                </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="frame">
                            <img src="./assets/img/carousel/img-1.png" alt="">
                            <div class="bottom-left content">
                                <div class="row">
                                    <div class="col-9 bg-white border-white">
                                        <h4 class="fw-bold">SALVIA</h4>
                                        <h5>Vida en crecimiento</h5>
                                        <p>Complejo de 7 lotes residenciales en Izamal,
                                            Yucatán. Cuentan con un tamaño de 375m2, 
                                            han aumentado un 16% su valor desde la venta 
                                            inicial debido a la plusvalía de la zona. 
                                            Inversionistas de este proyecto ya disfrutan 
                                            de sus rendimientos.
                                        </p>
                                    </div>
                                    <div class="col-3 bg-transparent d-flex align-items-center justify-content-start pt-5 ">
                                        <a class="" href=""> Ver desarrollo </a>   
                                    </div>
                                </div>
                                
                                
                            </div>
                         
                        </div>
                    </div>
                </div>
            </div>
            </div>
            
            <div class="row container-fluid justify-content-end my-5">
                <div class="col-8 col-md-6 col-lg-4 d-flex justify-content-end" id="arrows">
                    
                        <div class="prev d-flex justify-content-center align-items-center" id="prev">
                            <!-- <i class="fas fa-long-arrow-alt-left"></i> -->
                        </div>
                        <div class="next d-flex justify-content-center  align-items-center" id="next">
                            <!-- <i class="fas fa-long-arrow-alt-right"></i> -->
                        </div>
                        <div class="text-center d-flex justify-content-center  align-items-center"> 
                            Conocer más
                        </div>

                </div>
                
            </div>
              
        </article>
        <article class = "article-4">
            <div class="d-flex justify-content-center">
                <h4 class="h4 text-primary mb-5 l-sp-4">NOTICIAS EXLUSIVAS</h4>
            </div>
            <div class="row d-flex justify-content-end">
                <div class="col-12 col-md-10  order-last">
                    <!-- Slider main container -->
                    <div class="swiper-container swipper-1">
                        <div class="swiper-wrapper">
                            <!-- Slides -->
                            <div class="swiper-slide">
                                <div class="card swiper mb-3">
                                    <div class="row g-0">
                                        <div class="col-lg-7  order-lg-0">
                                            <div class="card-body d-flex flex-column justify-content-evenly">
                                                <div>
                                                    <p class="card-text">
                                                        <small class="text-white">
                                                            Mérida Yucatán
                                                        </small>
                                                    </p>
                                                </div>
                                                <div>
                                                    <h3 class="card-title">
                                                        3 razones para invertir en un desarrollo con FINSU
                                                    </h3>
                                                </div>
                                                <div>
                                                    <p class="card-text">
                                                        Invertir en el sector inmobiliario tiene un sinfín de beneficios, si a eso le sumamos el invertir en una ciudad como Mérida, que se encuentra en pleno auge inmobiliario, FINSU te da la mejor opción para invertir en la ciudad blanca por su sinfín de beneficios y rendimiento de inversión.                                
                                                    </p>
                                                </div>
                                                <div class="row justify-content-between align-items-center footer">
                                                    <div class="col-8 d-flex flex-wrap align-items-center   justify-content-between">
                                                        <p class="card-text">
                                                            <small class="text-primary">
                                                                <a href="blog.html">Leer más</a>
                                                            </small>   
                                                        </p>
                                                        <a href="blog.html">
                                                            <div class="arrow-host">
                                                                <div class="flecha"></div>
                                                            
                                                            </div>
                                                        </a>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-5 order-lg-1">
                                            <img src="./assets/img/cards/img-1.png" alt="...">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="card swiper mb-3">
                                    <div class="row g-0">
                                        <div class="col-lg-7 order-lg-0">
                                            <div class="card-body d-flex flex-column justify-content-evenly">
                                                <div>
                                                    <p class="card-text">
                                                        <small class="text-white">
                                                            Mérida Yucatán
                                                        </small>
                                                    </p>
                                                </div>
                                                <div>
                                                    <h3 class="card-title">
                                                        3 razones para invertir en un desarrollo con FINSU
                                                    </h3>
                                                </div>
                                                <div>
                                                    <p class="card-text">
                                                        Invertir en el sector inmobiliario tiene un sinfín de beneficios, si a eso le sumamos el invertir en una ciudad como Mérida, que se encuentra en pleno auge inmobiliario, FINSU te da la mejor opción para invertir en la ciudad blanca por su sinfín de beneficios y rendimiento de inversión.                                
                                                    </p>
                                                </div>
                                                <div class="row justify-content-between align-items-center footer">
                                                    <div class="col-8 d-flex flex-wrap align-items-center   justify-content-between">
                                                        <p class="card-text">
                                                            <small class="text-primary">
                                                                <a href="blog.html">Leer más</a>
                                                            </small>   
                                                        </p>
                                                        <a href="blog.html">
                                                            <div class="arrow-host">
                                                                <div class="flecha"></div>
                                                            
                                                            </div>
                                                        </a>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-5  order-lg-1">
                                        <img src="./assets/img/cards/img-1.png" alt="...">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="card swiper mb-3">
                                    <div class="row g-0">
                                        <div class="col-lg-7  order-lg-0">
                                            <div class="card-body d-flex flex-column justify-content-evenly">
                                                <div>
                                                    <p class="card-text">
                                                        <small class="text-white">
                                                            Mérida Yucatán
                                                        </small>
                                                    </p>
                                                </div>
                                                <div>
                                                    <h3 class="card-title">
                                                        3 razones para invertir en un desarrollo con FINSU
                                                    </h3>
                                                </div>
                                                <div>
                                                    <p class="card-text">
                                                        Invertir en el sector inmobiliario tiene un sinfín de beneficios, si a eso le sumamos el invertir en una ciudad como Mérida, que se encuentra en pleno auge inmobiliario, FINSU te da la mejor opción para invertir en la ciudad blanca por su sinfín de beneficios y rendimiento de inversión.                                
                                                    </p>
                                                </div>
                                                <div class="row justify-content-between align-items-center footer">
                                                    <div class="col-8 d-flex flex-wrap align-items-center   justify-content-between">
                                                        <p class="card-text">
                                                            <small class="text-primary">
                                                                <a href="blog.html">Leer más</a>
                                                            </small>   
                                                        </p>
                                                        <a href="blog.html">
                                                            <div class="arrow-host">
                                                                <div class="flecha"></div>
                                                            
                                                            </div>
                                                        </a>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-5 order-lg-1">
                                        <img src="./assets/img/cards/img-1.png" alt="...">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </article>
        <article class = "article-5">
            <div class="container">
                <div class="row">
                <div class="col"></div>
                <div class="col-sm-12 col-lg-6 d-flex justify-content-sm-center justify-content-lg-start align-items-center">
                    <div class="boxy">
                        <h4>¡EMPIEZA HOY!</h4>
                        <p>Intégrate a nuestro grupo exclusivo de inversionistas y recibe información única.
                        </p>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Escribe tu correo electrónico" aria-label="Recipient's username" aria-describedby="button-addon2">
                            <button class="btn btn-outline-secondary" type="button" id="button-addon2">Enviar</button>
                        </div>
                        <div class="form-check">
                            <input class="" type="radio" name="flexRadioDefault" id="flexRadioDefault1">
                            <label class="form-check-label" for="flexRadioDefault1">
                                <a href="">Acepto términos y condiciones</a> 
                            </label>
                          </div>
                    </div>
                </div>
                </div>
                
            </div>
        </article>  
        
        
        

    
    </main>
    <?php include 'footer.php';?>
    <?php include 'common-dependencies.php';?>
    <script src="https://unpkg.com/swiper/swiper-bundle.js"></script>
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script src="./js/sliders.js"></script>
</body>
</html>