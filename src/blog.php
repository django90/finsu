
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/plugins/slick-1.8.1/slick/slick.css">
    <link rel="stylesheet" href="assets/plugins/slick-1.8.1/slick/slick-theme.css">
    <link rel="stylesheet" href="css/style.css">
    <title>Finsu</title>
</head>
<body>  
    <header>
        <div class="parent-navbar">
            <?php include 'navbar.php';?>
        </div>
    </header>
    <main class="mt-5 container main">
        <div class="container row">
            <div class="col-12 col-md-6 col-lg-4">
                <article>
                    <div class="small">
                        <div class="img">
                            <img src="./assets/img/blog/img-1.png" alt="">
                        </div>
                        <div>
                            <h4 class="h4">Venden proyacto de inversión en Mérida</h4>
                            <p>
                                Somos un grupo de expertos en finanzas inmobiliarias dedicado a las invesiones inteligentes y seguras, ofrecemos los servicios de asesoramiento, crowfunding, construcció y ubanización podrás ser parte de nosotros en cualquiera de nuestros proyectos o inversiones                             
                            </p>
                        </div>
                    </div>
                </article>
                <article>
                    <div class="small">
                        <div class="img">
                            <img src="./assets/img/blog/img-2.png" alt="">
                        </div>
                        <div>
                            <h4 class="h4">Los desarrollos inmobiliarios que aumenta su plusvalia en el primer año</h4>
                            <p>
                                Somos un grupo de expertos en finanzas inmobiliarias dedicado a las invesiones inteligentes y seguras, ofrecemos los servicios de asesoramiento, crowfunding, construcció y ubanización podrás ser parte de nosotros en cualquiera de nuestros proyectos o inversiones 
                            </p>
                        </div>
                    </div>
                </article>
            </div>
            <div class="col-12 col-md-6 col-lg-4">
                <article>
                    <div class="medium">
                        <div class="img">
                            <img src="./assets/img/blog/img-3.png" alt="">
                        </div>
                        <div>
                            <h4 class="h4">Proyectos para invertir en Yucatán</h4>
                            <p>
                                Yucatán se ha convertido en uno de los mejores estados para invertir. Ha sido declarado Estado de la Paz por la Cumbre de Laureados Nobel en el 2019, cuentacon inversiones de empresas extranjeras de alto nivel, un Puerto de Altura que conecta rutas marítimas con otros países, un Aeropuerto Internacional y conexión ferroviaria. Yucatán se encuentra en la lista de las 5 ciudades con mejor oportunidad de inversión en el país. Contáctanos para conocer más opciones que te generen retornos de inversión.
                            </p>
                        </div>
                    </div>
                </article>
                <article>
                    <div class="medium">
                        <div class="img">
                            <img src="./assets/img/blog/img-4.png" alt="">
                        </div>
                        <div>
                            <h4 class="h4">¿Qué es la plusvalía?</h4>
                            <p>
                                Un término financiero que probablemente hayas escuchado. Este concepto está relacionado en mayor medida a activos como terrenos o viviendas los cuáles aumentan su valor y del cuál se consigue una ganancia.En activos inmobiliarios, estos aumentan su valor año tras año gracias su ubicación, al desarrollo de la zona por medio de zonas comerciales, residenciales, hospitales y poblacionales. Así que antes de realizar una inversión inmobiliaria, debes de tener en cuenta:-Ubicación: verifica la ciudad, las distancias con centros comerciales, hospitales, zonas residenciales. -Accesibilidad: Entre más accesible sea llegar, más plusvalía obtendrá en poco tiempo. -Planes de desarrollo: conoce qué planes de urbanización hay alrededor en un lapso de cinco a diez años. Contáctanos para recibir asesoría y comenzar tus planes de inversión
                            </p>
                        </div>
                    </div>
                </article>
            </div>
            <div class="col-12 col-md-6 col-lg-4">
                <article>
                    <div class="small reverse">
                        <div class="img">
                            <img src="./assets/img/blog/img-5.png" alt="">
                        </div>
                        <div>
                            <h4 class="h4">Construir vs Urbanizar</h4>
                            <p>
                                En FINSU contamos con los servicios de construcción y urbanización. Pueden surgir algunas dudas sobre sus diferencias y aquí te las resolveremos.Construir es edificar una obra con elementos necesarios, tomando en cuenta un plan previo. Es una excelente opción para verificar que se cumplan las normas técnicas básicas. Además de que es una excelente forma de asegurar la durabilidad y calidad del material y los cimientos dependiendo del terreno donde se realizará la construcción. Y lo mejor de esto, es que puedes ir de la mano con un especialista y diseñar la construcción de tus sueños. Urbanizar se relaciona en preparar un terreno para su uso urbano, es decir, abrir calles, preparar para la incursión de servicios como luz, agua y pavimento. Este tipo de proyecto se lleva a cabo para que se construya en el terreno a corto o mediano plazo. Contáctanos para recibir la mejor información
                            </p>
                        </div>
                    </div>
                </article>
                <article>
                    <div class="small reverse">
                        <div class="img">
                            <img src="./assets/img/blog/img-6.png" alt="">
                        </div>
                        <div class="info">
                            <h4 class="h4">¿Qué es el crowdfunding inmobiliario?</h4>
                            <p>
                                Un método único y nuevo para inversiones inmobiliarias, en las que se crea una red de pocas personas para invertir en proyectos inmobiliarios. Existen dos tipos de crowdfunding: “En deuda” y “En capital”. El primero se basa en invertir en un préstamo de una propiedad que te generará ingresos por medio de un pago de intereses a medida que el préstamo se regresa.El segundo tipo es invertir directamente en el proyecto y esperar el aumento de plusvalía para obtener retornos de inversión. Es una excelente opción para invertir por primera vez y con poco dinero Forma parte de nuestra comunidad de inversionistas, contáctanos
                            </p>
                        </div>
                    </div>
                </article>
            </div>
        </div>    
    </main>
    <?php include 'footer.php';?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <script src="./js/funcionalidades.js"></script>
</body>
</html>