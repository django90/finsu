<footer>
    <div class="container pt-5">
        <div class="row">
            <div class="row">
                <div class="col">
                    <div class="d-flex justify-content-start">
                        <img class="logo" src="./assets/img/finsu-white.svg" alt="">
                    </div>
                </div>
                <div class="col">
                    <div class="row flex-wrap justify-content-between align-items-center">
                        <div class="col">
                            <h2 class="text-center">Contáctanos</h2>
                        </div>
                        <div class="col d-flex justify-content-around redes">
                            <a class="icon-facebook" href=""></a>
                            <a class="icon-instagram" href=""></a>
                            <a class="icon-linkedin2" href=""></a>
                            <a class = "icon-skype" href=""></a>
                            <a class = "icon-whatsapp" href=""></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12  col-lg-6 order-1 order-lg-0">
                    <div class="row py-5 py-lg-0">
                        <div class="row">
                            <div class="col">
                                <div>
                                    <h5 class="text-white my-2">Soporte</h5>
                                    <ul>
                                        <li><a href="">Solicitar cita</a></li>
                                        <li><a href="">Agendar llamada</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col">
                                <div>
                                    <h5 class="text-white my-2">Preguntas frecuentes</h5>
                                    <div class="row">
                                        <ul class="col-12 col-sm-6">
                                            <li><a href=""> ¿Cómo empiezo?</a></li>
                                            <li><a href=""> ¿Qué inversión elegir?</a></li>
                                        </ul>
                                        <ul class="col-12 col-sm-6">
                                            <li><a href=""> Seguridad de FINSU</a></li>
                                            <li><a href="">¿Es tu primera inversión?</a> </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div>
                                    <h5 class="text-white my-2">Servicios</h5>
                                    <ul>
                                        <li><a href=""> Asesoriamiento</a></li>
                                        <li><a href="">Crowdfunding</a> </li>
                                        <li><a href="">Urbanización</a> </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col">
                                <div>
                                    <h5 class="text-white my-2">Información</h5>
                                    <ul>
                                        <li><a href="">Horarios de atención</a> </li>
                                        <li><a href="">Oficinas</a> </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12  col-lg-6 order-0 order-lg-1">
                    
                    <div class="row">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <label for="exampleFormControlInput1" class="form-label text-white">Nombre</label>
                                <input type="text" class="form-control" id="name" placeholder="Nombre completo">
                            </div>
                            <div class="col-12 col-md-6">
                                <label for="exampleFormControlInput1" class="form-label text-white">Teléfono</label>
                                <input type="tel" class="form-control" id="phone" pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}" placeholder="998 012 3456">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <label for="exampleFormControlInput1" class="form-label text-white">Cómo te enteraste de nosotros</label>
                                <input type="text" class="form-control" id="informacionContacto" placeholder="Cuéntanos">
                            </div>
                            <div class="col-12 col-md-6">
                                <label for="exampleFormControlInput1" class="form-label text-white">Mail</label>
                                <input type="email" class="form-control" id="email" placeholder="correo electrónico">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label for="exampleFormControlTextarea1" class="form-label text-white">Mensaje</label>
                                <textarea class="form-control" id="mensaje" rows="3"></textarea>
                                <div class="d-grid gap-2">
                                    <button class="btn btn-primary" type="button" id="submit">Enviar</button>
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </div>
        
    </div>
    <div class="container-fluid row"></div>
</footer>