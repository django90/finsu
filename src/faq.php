
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <title>Finsu</title>
</head>
<body>  
  <header class="">
        <div class="parent-navbar">
            <?php include 'navbar.php';?>
        </div>
    </header>

    <main class="mt-5 faq-main main"> 
      <div class="img-faq-background">
            <img src="./assets/img/faqs/header.png" alt="">
        </div>
        <div class="container">
            <div class="accordion" id="accordionPanelsStayOpenExample">
                <div class="accordion-item">
                  <h2 class="accordion-header" id="panelsStayOpen-headingOne">
                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true" aria-controls="panelsStayOpen-collapseOne">
                      <div class="d-flex align-items-center">
                          <h4 class="h4 text-primary">01</h4>
                          <h5 class="h5 text-dark ps-2">¿Cómo empiezo?</h5>
                      </div>
                      
                    </button>
                  </h2>
                  <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
                    <div class="accordion-body">
                        <p>ofrecemos los servicios de asesoramiento, crowfunding, construcció y ubanización podrás ser parte de nosotros en cualquiera de nuestros proyectos o inversiones </p>
                    </div>
                  </div>
                </div>
                <div class="accordion-item">
                  <h2 class="accordion-header" id="panelsStayOpen-headingTwo">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="false" aria-controls="panelsStayOpen-collapseTwo">
                        <div class="d-flex align-items-center">
                            <h4 class="h4 text-primary">02</h4>
                            <h5 class="h5 text-dark ps-2">Seguridad de FINSU</h5>
                        </div>                    </button>
                  </h2>
                  <div id="panelsStayOpen-collapseTwo" class="accordion-collapse collapse" aria-labelledby="panelsStayOpen-headingTwo">
                    <div class="accordion-body">
                        <p>ofrecemos los servicios de asesoramiento, crowfunding, construcció y ubanización podrás ser parte de nosotros en cualquiera de nuestros proyectos o inversiones </p>
                    </div>
                  </div>
                </div>
                <div class="accordion-item">
                  <h2 class="accordion-header" id="panelsStayOpen-headingThree">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseThree" aria-expanded="false" aria-controls="panelsStayOpen-collapseThree">
                        <div class="d-flex align-items-center">
                            <h4 class="h4 text-primary">03</h4>
                            <h5 class="h5 text-dark ps-2">¿Qué inversión elegir?</h5>
                        </div>
                    </button>
                  </h2>
                  <div id="panelsStayOpen-collapseThree" class="accordion-collapse collapse" aria-labelledby="panelsStayOpen-headingThree">
                    <div class="accordion-body">
                        <p>ofrecemos los servicios de asesoramiento, crowfunding, construcció y ubanización podrás ser parte de nosotros en cualquiera de nuestros proyectos o inversiones </p>
                    </div>
                  </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="panelsStayOpen-headingFour">
                      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseFour" aria-expanded="false" aria-controls="panelsStayOpen-collapseFour">
                        <div class="d-flex align-items-center">
                            <h4 class="h4 text-primary">04</h4>
                            <h5 class="h5 text-dark ps-2">¿Es tu primera inversión?</h5>
                        </div>
                      </button>
                    </h2>
                    <div id="panelsStayOpen-collapseFour" class="accordion-collapse collapse" aria-labelledby="panelsStayOpen-headingFour">
                      <div class="accordion-body">
                        <p>ofrecemos los servicios de asesoramiento, crowfunding, construcció y ubanización podrás ser parte de nosotros en cualquiera de nuestros proyectos o inversiones </p>
                      </div>
                    </div>
                  </div>
            </div>
        </div>
        
    </main>
    <?php include 'footer.php';?>
    <?php include 'common-dependencies.php';?>
</body>
</html>