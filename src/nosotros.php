
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/plugins/slick-1.8.1/slick/slick.css">
    <link rel="stylesheet" href="assets/plugins/slick-1.8.1/slick/slick-theme.css">
    <link rel="stylesheet" href="css/style.css">
    <title>Finsu</title>
</head>
<body>  
    <header class="">
        <div class="parent-navbar">
            <?php include 'navbar.php';?>
        </div>
       
    </header>
    <main class="my-5 main">
        <article class="container position-relative overflow-hidden" id="line-down">
            <div class="background-nosotros">
                <img src="./assets/img/nosotros/Group.svg" alt="">
            </div>
            <div class="d-flex flex-md-row flex-column contenido-nosotros">
                <div class="col-md-5 d-flex flex-column text-align-start mb-5 p-3 pe-5">
                    <h2 class="h2">
                        Acerca de nosotros
                    </h2>
                    <h6 class="h6 mb-3 text-primary">
                        NUESTRO COMPROMISO ES CONTIGO
                    </h6>
                    <div class="mt-3">
                        <h5 class="h5">
                            Somos expertos en incrementar tus inversiones
                        </h5>
                        <p class="fs-6">
                            Hemos creado proyectos con exito y echo feliz a muchos socios con sus resultados, con nosotros podrás invertir de una manera segura gracias a nuestro respaldo claro y legal.
                        </p>
                    </div>
                    <div class="mt-3">
                        <h5 class="h5">
                            Tenemos los proyectos indicados
                        </h5>
                        <p class="fs-6">
                            Nustros poryectos son en el área inmobiliaria el cual puedes ser parte con pequeñas inversiones en tiempo determinados con grades resultados.
                        </p>
                    </div>
                    <div class="mt-3">
                        <h5 class="h5">
                            Ayudamos y guiamos a nuestros inversionistas
                        </h5>
                        <p class="fs-6">
                            Gracias a la confianza y uniones que tenemso podemos brindar las mejores decisiones de inversión para ti y tus proyectos.
                        </p>
                    </div>
                </div>
                <div class="col-md-7 d-flex justify-content-around" id="images-nosotros">
                    <div class="img-host align-self-center align-self-md-start">
                        <img src="./assets/img/nosotros/img-1.png" alt="">
                    </div>
                    <div class="img-hos align-self-center align-self-md-center align-self-lg-end">
                        <img src="./assets/img/nosotros/img-2.png" alt="">
                    </div>
                </div>
            </div>
        </article>
        <section>
            <article class="container" id="section-nosotros">
                <div class="d-flex flex-wrap">
                    <div class="col-12 col-md-4 p-3">
                        <h3 class="h3">¿Qué hacemos en FINSU?</h3>
                    </div>
                    <div class="col-12 col-md-4 p-3">
                        <p>
                            Somos un grupo de expertos en finanzas inmobiliarias dedicado a las invesiones inteligentes y seguras, ofrecemos los servicios de <span class="text-primary">asesoramiento, crowfunding, construcción y ubanización </span> podrás ser parte de nosotros en cualquiera de nuestros proyectos o inversiones 
                        </p>
                    </div>
                    <div class="col-12 col-md-4 p-3">
                        <p>
                            Nuestra formula del éxito esta en la confianza de nuestros socios, te ofrecemos planes de inversión a plazos razonables con toda la claridad posible para la seguridad de tu dinero y futuro.
                        </p>
                    </div>
                </div>
            </article>
            <article>
                <div class="parent-nosotros">
                    <div class="child-1">    
                        <img src="./assets/img/nosotros/img-3.png" alt="">
                        <div class="card">
                            <div class="first">
                                <p>
                                    Nuestra razón la cual  hacemos lo que hacemos es por inversionistas / socios que desean una manera segura y fácil de invertir para un futuro con increíbles resultados                                 </p>
                            </div>
                        </div>                
                    </div>
                </div>
            </article>
        </section>
    </main>
    <?php include 'footer.php';?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <script src="./js/funcionalidades.js"></script>

</body>
</html>