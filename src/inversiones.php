<!DOCTYPE html>
<html lang="es">
    <?php include 'head.php';?>
<body>  
    <header class="">
        <div class="parent-navbar">
            <?php include 'navbar.php';?>
        </div>
        
    </header>
    <section class="main">
        <div class="img-inversiones-background">
            <div class="outer-box">
                <div class="caja">
                    <h3 class="h3">
                        Imagina poder invertir de una manera fácil y solo disfrutar tus ganancias
                    </h3>
                    <h5 >
                        Contamos con este nuevo proyecto en el cual solo podrán participar 120 inversionistas con comodas inversiones y planes seguros.
                    </h6>
                    <button type="button" class="btn btn-primary btn-lg">
                        <div>Iniciar inversion</div>
                    </button>
                </div>
            </div>
        </div>
    </section>
    <div class="orangy-line"></div>
    <main class="mt-5">
        <article class="article " id="article-1-inversion">
            <div class="d-flex flex-wrap">
                <div class="col-12 col-md-7">
                    <img src="./assets/img/inversiones/img-1.png" alt="">
                </div>
                <div class="col-12 col-md-5 mt-5 align-self-center">
                    <div class="d-flex flex-column align-items-center align-items-md-start host">
                        <div id="bird-frame">
                            <img src="./assets/img/carousel/salvia.svg" alt="">
                        </div>
                        <h4 class="h6 text-primary">
                            NUESTRO ÉXITO
                        </h6>
                        <h5 class="h5 mt-3">Vida en crecimiento</h5>
                        <p class="text-center text-md-start">
                            Este complejo contó con 7 lotes de 375 metros cuadrados, en Izamal, Yucatán, cada lote a subido hasta un 16% de su valor inicial por plusvalía dando buenos resultados en rendimiento de inversión.
                        </p>
                        <button type="button" class="btn btn-primary btn-lg">
                            <div>Ver desarrollo</div>
                        </button>

                    </div>
                </div>
            </div>
        </article>
        <article class = "article-4">
            <div class="d-flex justify-content-center">
                <h4 class="h4 text-primary mb-5 l-sp-4">Noticias exclusivas</h4>
            </div>
            <div class="row d-flex justify-content-end">
                <div class="col-12 col-md-10  order-last">
                    <!-- Slider main container -->
                    <div class="swiper-container swipper-3">
                        <div class="swiper-wrapper">
                            <!-- Slides -->
                            <div class="swiper-slide">
                                <div class="card swiper mb-3">
                                    <div class="row g-0">
                                        <div class="col-6 col-md-7 order-1 order-md-0">
                                            <div class="card-body d-flex flex-column justify-content-evenly">
                                                <div>
                                                    <p class="card-text">
                                                        <small class="text-white">
                                                            Mérida Yucatán
                                                        </small>
                                                    </p>
                                                </div>
                                                <div>
                                                    <h3 class="card-title">
                                                        3 razones para invertir en un desarrollo con FINSU
                                                    </h3>
                                                </div>
                                                <div>
                                                    <p class="card-text">
                                                        Invertir en el sector inmobiliario tiene un sinfín de beneficios, si a eso le sumamos el invertir en una ciudad como Mérida, que se encuentra en pleno auge inmobiliario, FINSU te da la mejor opción para invertir en la ciudad blanca por su sinfín de beneficios y rendimiento de inversión.                                
                                                    </p>
                                                </div>
                                                <div class="row justify-content-between align-items-center footer">
                                                    <div class="col-8 d-flex flex-wrap align-items-center   justify-content-between">
                                                        <p class="card-text">
                                                            <small class="text-primary">
                                                                <a href="blog.html">Leer más</a>
                                                            </small>   
                                                        </p>
                                                        <a href="blog.html">
                                                            <div class="arrow-host">
                                                                <div class="flecha"></div>
                                                            
                                                            </div>
                                                        </a>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6 col-md-5  order-0 order-md-1">
                                            <img src="./assets/img/cards/img-1.png" alt="...">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="card swiper mb-3">
                                    <div class="row g-0">
                                        <div class="col-6 col-md-7  order-1 order-md-0">
                                            <div class="card-body d-flex flex-column justify-content-evenly">
                                                <div>
                                                    <p class="card-text">
                                                        <small class="text-white">
                                                            Mérida Yucatán
                                                        </small>
                                                    </p>
                                                </div>
                                                <div>
                                                    <h3 class="card-title">
                                                        3 razones para invertir en un desarrollo con FINSU
                                                    </h3>
                                                </div>
                                                <div>
                                                    <p class="card-text">
                                                        Invertir en el sector inmobiliario tiene un sinfín de beneficios, si a eso le sumamos el invertir en una ciudad como Mérida, que se encuentra en pleno auge inmobiliario, FINSU te da la mejor opción para invertir en la ciudad blanca por su sinfín de beneficios y rendimiento de inversión.                                
                                                    </p>
                                                </div>
                                                <div class="row justify-content-between align-items-center footer">
                                                    <div class="col-8 d-flex flex-wrap align-items-center   justify-content-between">
                                                        <p class="card-text">
                                                            <small class="text-primary">
                                                                <a href="blog.html">Leer más</a>
                                                            </small>   
                                                        </p>
                                                        <a href="blog.html">
                                                            <div class="arrow-host">
                                                                <div class="flecha"></div>
                                                            
                                                            </div>
                                                        </a>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6 col-md-5  order-0 order-md-1">
                                        <img src="./assets/img/cards/img-1.png" alt="...">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="card swiper mb-3">
                                    <div class="row g-0">
                                        <div class="col-6 col-md-7  order-1 order-md-0">
                                            <div class="card-body d-flex flex-column justify-content-evenly">
                                                <div>
                                                    <p class="card-text">
                                                        <small class="text-white">
                                                            Mérida Yucatán
                                                        </small>
                                                    </p>
                                                </div>
                                                <div>
                                                    <h3 class="card-title">
                                                        3 razones para invertir en un desarrollo con FINSU
                                                    </h3>
                                                </div>
                                                <div>
                                                    <p class="card-text">
                                                        Invertir en el sector inmobiliario tiene un sinfín de beneficios, si a eso le sumamos el invertir en una ciudad como Mérida, que se encuentra en pleno auge inmobiliario, FINSU te da la mejor opción para invertir en la ciudad blanca por su sinfín de beneficios y rendimiento de inversión.                                
                                                    </p>
                                                </div>
                                                <div class="row justify-content-between align-items-center footer">
                                                    <div class="col-8 d-flex flex-wrap align-items-center   justify-content-between">
                                                        <p class="card-text">
                                                            <small class="text-primary">
                                                                <a href="blog.html">Leer más</a>
                                                            </small>   
                                                        </p>
                                                        <a href="blog.html">
                                                            <div class="arrow-host">
                                                                <div class="flecha"></div>
                                                            
                                                            </div>
                                                        </a>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6 col-md-5  order-0 order-md-1">
                                        <img src="./assets/img/cards/img-1.png" alt="...">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </article>
    </main>
    <?php include 'footer.php';?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <script src="./js/funcionalidades.js"></script>
    <script src="https://unpkg.com/swiper/swiper-bundle.js"></script>
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script src="./js/sliders.js"></script>
</body>
</html>