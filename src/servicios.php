
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/plugins/slick-1.8.1/slick/slick.css">
    <link rel="stylesheet" href="assets/plugins/slick-1.8.1/slick/slick-theme.css">
    <link rel="stylesheet" href="css/style.css">
    <title>Finsu</title>
</head>
<body>  
    <header>
        <div class="parent-navbar">
            <?php include 'navbar.php';?>
        </div>
    </header>
    <main class="mt-5 main">
        <article class="article">
            <div class="parent-servicios">
                <div class="child-1">    
                    <img src="./assets/img/servicios/img-1.png" alt="">
                    <div class="card">
                        
                        <div class="ps-5  first d-flex flex-column justify-content-evenly align-items-start">
                            <div class="iconos">
                                
                            </div>
                            <div class=" d-flex flex-column justify-content-evenly align-items-start">
                                <h3 class="pb-3">Asesoramiento</h3>
                                <p class="text-muted  d-flex justify-content-start">
                                    Asesoramos a nuestros clientes para que tomen la mejor decisión redituable
                                </p>
                            </div>
                            
                            <a href ="inversiones.php" type="button" class="btn btn-primary py-3">
                                <div class="append-greater-than" >
                                    Empezar 
                                </div>
                            </a>
                        </div>
                    </div>                
                    
                </div>
                <div class="child-2">
                    <div class="d-flex flex-wrap">
                        <div class="col-12 col-md-6">
                            <p class="p-5">
                                Nuestros asesoramientos están enfocados en inversiones inmobiliarias, construcción y urbanización, si estás apunto de tomar una decisión en inversión contáctanos nosotros te ayudamos a tomar el mejor camino.
                            </p>
                        </div>
                        <div class="col-12- col-md-6">
                            <p class="p-5" >
                                Nuestro grupo de asesores podrá guiarte a la toma de decisiones para que tengas claro tus objetivos como inversionista o socio.
                            </p>
                        </div>
                    </div>
                </div> 
            </div>
        </article>
        <article class="article">
            <div class="parent-servicios">
                <div class="child-1">    
                    <img src="./assets/img/servicios/img-2.png" alt="">
                    <div class="card">
                        <div class="ps-5  second  d-flex flex-column justify-content-evenly align-items-start">
                            <div class="iconos"></div>
                            <div class=" d-flex flex-column justify-content-evenly align-items-start">
                                <h3 class="pb-3 ">Crowdfunding</h3>
                                <p class="text-muted ">
                                    Inversiones atractivas, tu eliges a tus socios
                                </p>
                            </div>
                            
                            <a href ="inversiones.php" type="button" class="btn btn-primary  py-3">
                                <div class="append-greater-than" >
                                    Empezar
                                </div>
                            </a>
                        </div>
                    </div>                
                </div>
                <div class="child-2">
                    <div class="d-flex flex-wrap">
                        <div class="col-12 col-md-6">
                            <p class="p-5">
                                Con información básica sobre tus proyectos de inversión, nos encargamos de presentarte socios que te ayudarán a crecer exitosamente.                             </p>
                        </div>
                        <div class="col-12- col-md-6">
                            <p class="p-5" >
                                Tener claros tus objetivos de inversión es la forma correcta de encontrar personas que tengan las mismas ideas que tú.                             </p>
                        </div>
                    </div>
                </div>
            </div>
        </article>
        <article class="article last">
            <div class="parent-servicios">
                <div class="child-1">
                    <img src="./assets/img/servicios/img-3.png" alt="">
                    <div class="card ">
                        <div class="ps-5  third   d-flex flex-column justify-content-evenly align-items-start">
                            <div class="iconos"></div>
                            <div class="  d-flex flex-column justify-content-evenly align-items-start">
                                <h3 class="pb-3 ">Construcción y urbanización</h3>
                                <p class="text-muted ">
                                    Convierte tu sueño en realidad                            
                                </p>
                            </div>
                            
                            <a href ="inversiones.php" type="button" class="btn btn-primary  py-3">
                                <div class="append-greater-than" >
                                    Empezar  
                                </div>
                            </a>
                        </div>
                    </div>  
                </div>
                <div class="child-2">
                    <div class="d-flex flex-wrap">
                        <div class="col-12 col-md-6">
                            <p class="p-5">
                                La construcción es un área que debe de contar con cimientos fuertes y sin errores para contar con un futuro de calidad.                             
                            </p>
                        </div>
                        <div class="col-12- col-md-6">
                            <p class="p-5" >

                                Nuestra trayectoria es amplia e internacional, nuestras urbanizaciones son de alta calidad. 

                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </article>   
    </main>
    <?php include 'footer.php';?>
    <?php include 'common-dependencies.php';?>

</body>
</html>